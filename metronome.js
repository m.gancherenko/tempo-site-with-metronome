window.addEventListener('load', init);

function init() {
  // vars
  var led = new LED(document.querySelector('.led')),
      bpmLabel = document.querySelector('.bpm-label'),
      bpm = document.querySelector('.bpm-input'),
      stop = document.querySelectorAll('.right-side'),
      block = document.querySelectorAll(".block"),
      intervalReference = null,
      context = new (window.AudioContext || window.webkitAudioContext)();

  // events
  bpm.addEventListener('change', e => {
    bpmLabel.innerText = bpm.value + ' BPM';
    clearInterval(intervalReference);
    intervalReference = setInterval(tick, getDelay());
  });
  
  block.forEach(elm =>{
    elm.addEventListener('click', ()=>{
		
		 let speed = elm.getAttribute('data-attr');
		 
		
		 
		 if (intervalReference) {
              clearInterval(intervalReference);
              intervalReference = null;

               let blc = document.querySelectorAll(".block");
              blc.forEach(bl=>{
                  if(bl.getAttribute('class')=="block absolute active"){
                      bl.removeAttribute('class')
                      bl.setAttribute('class', 'block absolute');
                    
                   	let push = bl.closest(".block");
                   	let first = push.children[0];
                    let second = push.children[1];
                   	pause(first);
		            slideLeft(second);
                  
                  }
              })
        	  
            }
                else{
                    intervalReference = setInterval(tick, getDelay(speed));
                    elm.classList.add('active');
                    }
	});
	
  })
  
  // start up the metronome
  // intervalReference = setInterval(tick, getDelay(speed));


  ///////////////////////////////////
  
  
  
  
  function getDelay(speed) {
    return 1000*60 / speed;
    
    
  }
  
  function togglePlay() {
    if (intervalReference) {
      clearInterval(intervalReference);
      intervalReference = null;
       
    }
    else
      intervalReference = setInterval(tick, getDelay());
     
    
  }

  function tick() {
    if (!led.isLit()) {
      var now = context.currentTime,
          click = new Kick(context);
          //click = new Hat(context);
      click.trigger(now);
      setTimeout(tick, 100);
    }
    led.toggle();
  }
}





// - - - - - - - - - - - - -
// - - classes - - - - - - -
// - - - - - - - - - - - - -

class LED {
  constructor(el) {
    this.el = el;
    this.lit = false;
  }
  
  isLit() {
    return this.lit;
  }
  
  toggle() {
    // check for classList property
    if (this.el.classList) {
      this.el.classList[ this.lit ? 'remove' : 'add' ]('lit');
    }
    // use className instead
    else {
      // remove .lit
      if (this.lit)
        this.el.className = this.el.className.replace('lit', '');
      // add .lit
      else
        this.el.className += ' lit';
    }
    
    // toggle the lit property
    this.lit = !this.lit;
  }
}

class Beat {
  constructor(context) {
    // store the audio context
    this.context = context;
  }
  
  setup() {
    this.osc = this.context.createOscillator();
    this.gain = this.context.createGain();
    this.osc.connect(this.gain);
    this.gain.connect(this.context.destination);
  }
}

class Kick extends Beat {
  trigger(time) {
    this.setup();
    
    // set initial frequency and gain (volume) of attack
    this.osc.frequency.setValueAtTime(120, time);
    this.gain.gain.setValueAtTime(1, time);
    
    // drop frequency of the oscillator rapidly after the initial attack
    this.osc.frequency.exponentialRampToValueAtTime(0.001, time + .5);
    this.gain.gain.exponentialRampToValueAtTime(0.001, time + .5);
    
    // fire the oscillator and set stop time                                            
    this.osc.start(time);
    this.osc.stop(time + 0.5);
  }
}

// from - http://joesul.li/van/synthesizing-hi-hats/
class Hat extends Beat {
  trigger(time) {
    this.setup();
    
    var ctx = this.context,
      fundamental = 40,
      ratios = [2, 3, 4.16, 5.43, 6.79, 8.21];
    
    var bandpass = ctx.createBiquadFilter();
    bandpass.type = 'bandpass';
    bandpass.frequency.value = 13000;

    // highpass
    var highpass = ctx.createBiquadFilter();
    highpass.type = 'highpass';
    highpass.frequency.value = 7000;

    // connect the graph
    bandpass.connect(highpass);
    highpass.connect(this.gain);

    // skip the default oscillator and create several more
    ratios.forEach(ratio => {
      var osc = ctx.createOscillator();
      osc.type = 'square';
      
      // frequency is the fundamental * this oscillator's ratio
      osc.frequency.value = fundamental * ratio;
      osc.connect(bandpass);
      osc.start(time);
      osc.stop(time + 0.3);
    });

    // define the volume envelope
    this.gain.gain.setValueAtTime(0.00001, time);
    this.gain.gain.exponentialRampToValueAtTime(1, time + 0.02);
    this.gain.gain.exponentialRampToValueAtTime(0.3, time + 0.03);
    this.gain.gain.exponentialRampToValueAtTime(0.00001, time + 0.3); 
  }
}




document.addEventListener("click", (e) => {
	

	let act = document.querySelector(".active");
	if (act !== null) {
		act.classList.remove(".active");
	} else {
	}

	let push = e.target.closest(".block");

	if (push == null) return;

// 	
	let first = push.children[0];
	let second = push.children[1];
	if (push.getAttribute("class") === "block absolute active") {
		start(first);

		slideRight(second);
	} else {
		pause(first);
		slideLeft(second);
	}
});




function start(first) {
	first.animate(
		[
			{
				borderLeft: "5px  solid #ff5a00",
				borderRight: "5px  solid #ff5a00",
				borderTop: "10px  solid #ff5a00",
				borderBottom: "10px  solid #ff5a00"
			}
		],
		{
			// timing options
			duration: 400,
			fill: "forwards",
			iteration: 1
		}
	);
}

function pause(first) {
	first.animate(
		[
			{
				borderLeft: "5px  solid #ff5a00",
				borderRight: "5px  solid #ff5a00",
				borderTop: "10px  solid #ff5a00",
				borderBottom: "10px  solid #ff5a00"
			},
			{
				borderLeft: " 15px solid #ff5a00",
				borderRight: "0px transparent",
				borderTop: "10px solid transparent",
				borderBottom: "10px solid transparent"
			}
		],
		{
			// timing options
			delay: 200,
			duration: 400,
			fill: "forwards",
			iteration: 1
		}
	);
}

function slideRight(second) {
	second.animate(
		[
			{
				left: "0px",
				opacity: "0"
			},
			{
				left: "15px",
				opacity: "1"
			}
		],
		{
			// timing options
			delay: 200,
			duration: 400,
			fill: "forwards",
			iteration: 1
		}
	);
}

function slideLeft(second) {
	second.animate(
		[
			{
				left: "0px",
				opacity: "0"
			}
		],
		{
			// timing options
			duration: 200,
			fill: "forwards",
			iteration: 1
		}
	);
}



